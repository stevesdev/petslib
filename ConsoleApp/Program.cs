﻿using LibPets.Infrastructure;
using LibPets.Interfaces;
using LibPets.Models;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ConsoleApp
{
    /// <summary>
    /// Simple .net Core 2.0 Console Project utilizing LibPets via DI & output results.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            //Demonstrate DI
            var svcProvider = new ServiceCollection()
                .AddSingleton<IContextProvider<Person>, JsonApiProvider<Person>>()
                .AddSingleton<ILibPetsRepository<Pet>, PersonalPetsRepository>()
                .BuildServiceProvider();

            var repo = svcProvider.GetService<ILibPetsRepository<Pet>>();

            doPetWork(repo);

            Console.ReadKey();
        }

        private static void doPetWork(ILibPetsRepository<Pet> repo)
        {
            var resultset = repo.GetTGroupedSorted("Cat").Result;

            //Iterate results to Console
            foreach (var kvp in resultset)
            {
                Console.WriteLine(kvp.Key);
                foreach (var pet in kvp.Value)
                {
                    Console.WriteLine(" - {0}", pet.Name);
                }
            }
        }
    }
}
