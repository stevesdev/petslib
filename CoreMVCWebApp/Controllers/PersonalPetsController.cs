﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LibPets.Interfaces;
using LibPets.Models;

namespace CoreMVCWebApp.Controllers
{
    public class PersonalPetsController : Controller
    {
        private ILibPetsRepository<Pet> _ppRepo;

        public PersonalPetsController(ILibPetsRepository<Pet> ppRepo)
        {
            _ppRepo = ppRepo;
        }

        public async Task<IActionResult> Index()
        {
            var model = await _ppRepo.GetTGroupedSorted("Cat");
            return View("ViewPets", model);
        }

        public async Task<IActionResult> ViewType(string id = "Cat")
        {
            var model = await _ppRepo.GetTGroupedSorted(id);
            return View("ViewPets", model);
        }
    }
}