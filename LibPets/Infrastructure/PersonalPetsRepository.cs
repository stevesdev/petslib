﻿using LibPets.Interfaces;
using LibPets.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibPets.Infrastructure
{
    public class PersonalPetsRepository : ILibPetsRepository<Pet>
    {

        private IContextProvider<Person> _contextProvider;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contextProvider">Required for source data [IApiProvider]</param>
        public PersonalPetsRepository(IContextProvider<Person> contextProvider) => _contextProvider = contextProvider;


        /// <summary>
        /// Calls the apiprovider (datasource) and returns Lists of pets according to owners gender
        /// </summary>
        /// <param name="petType">Default = "Cat"</param>
        /// <returns>Sorted Dictionary of Owners 'Gender' with value of list contains PET model ordered A-Z</returns>
        public async Task<IDictionary<string, List<Pet>>> GetTGroupedSorted(string arg = "Cat")
        {
            var allData = await _contextProvider.GetAll();
            var petData = allData.Where(x => x.Pets != null)
                .GroupBy(x => x.Gender)
                .ToDictionary(g => g.Key, g => g.SelectMany(t => t.Pets)
                .Where(p => p.Type.Equals(arg))
                .OrderBy(p => p.Name)
                .ToList());

            return petData; 
        }
    }
}
