﻿using LibPets.Interfaces;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace LibPets.Infrastructure
{
    public class JsonApiProvider<T> : IContextProvider<T>
    {
        private static string _uri;

        /// <summary>
        /// ApiProvider Constructor
        /// Contains live default
        /// provide uri if alternate url required.
        /// </summary>
        /// <param name="uri">URI to query for json</param>
        public JsonApiProvider(string uri = @"http://agl-developer-test.azurewebsites.net/people.json") => _uri = uri;

        /// <summary>
        /// Fetch, return IEnumerable object of T via an external API
        /// </summary>
        /// <returns>All records of T from api.</returns>
        //public IList<T> GetAll()
        //{
        //    var jsonTask = httpFetchJson(_uri);
        //    var jsondata = jsonTask.Result;

        //    return JsonConvert.DeserializeObject<List<T>>(jsondata); 
        //}

        public async Task<IList<T>> GetAll()
        {
            var jsonTaskData = await httpFetchJson(_uri);
            //Deserialize & return
            return JsonConvert.DeserializeObject<List<T>>(jsonTaskData);
        }

        private static async Task<string> httpFetchJson(string uri = @"http://agl-developer-test.azurewebsites.net/people.json")
        {
            using (var client = new HttpClient())
            {
                return await client.GetStringAsync(_uri);
            }
        }

    }
}

/*
 * 
 *         public IEnumerable<T> GetAll()
        {
            var t = HttpFetchJson<IEnumerable<T>>(_uri);
            return t.Result;
        }

        public static async Task<T> HttpFetchJson<T>(string uri = @"http://agl-developer-test.azurewebsites.net/people.json")
        {
            var client = new HttpClient();
            var task = client.GetStringAsync(uri);
            var result = await task;
            var deserialized = JsonConvert.DeserializeObject<T>(result);
            //PetsDataCollection petsdata = //= JsonConvert.DeserializeObject<PetsDataCollection>(result);
            //    JsonConvert.DeserializeObject<List<Person>(result);
            return deserialized;
        }
*/