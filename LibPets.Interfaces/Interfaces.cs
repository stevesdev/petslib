﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LibPets.Interfaces
{

    public interface ILibPetsRepository<T>
    {

        /// <summary>
        /// Gets T as a Dictionary Grouped by(key) of Sorted(Values)
        /// </summary>
        /// <param name="arg">to filter by</param>
        /// <returns>Grouped by(key) Dictionary of Sorted(Values)</returns>
        Task<IDictionary<string, List<T>>> GetTGroupedSorted(string arg);
    }

    public interface IContextProvider<T>
    {
        //IList<T> GetAll();

        Task<IList<T>> GetAll();
    }
}
