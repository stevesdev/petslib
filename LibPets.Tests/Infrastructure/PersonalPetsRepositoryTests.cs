using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LibPets.Infrastructure;
using LibPets.Interfaces;
using LibPets.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Moq;

namespace LibPets.Tests.Infrastructure
{
    [TestClass]
    public class PersonalPetsRepositoryTests
    {
        private MockRepository mockRepository;

        private Mock<IContextProvider<Person>> mockContextProvider;

        private List<Person> testData = null;

        [TestInitialize]
        public void TestInitialize()
        {
            this.mockRepository = new MockRepository(MockBehavior.Strict);

            this.mockContextProvider = this.mockRepository.Create<IContextProvider<Person>>();
            //setup test json object (will be supplied to mock object)
            var jsonTestData = File.ReadAllText("TestData\\PersonTestData.json");
            testData = JsonConvert.DeserializeObject<List<Person>>(jsonTestData);

            Assert.IsNotNull(testData, "Test Data Object (List<Person>) for mockContextProvider was null!");
        }

        [TestCleanup]
        public void TestCleanup()
        {
            //this.mockRepository.VerifyAll();
        }

        /// <summary>
        /// Series of Assertions test the validity of the GetTGroupedSorted in the PersonalPetsRepository
        /// agains the required spec listed in the scope of the project.
        /// </summary>
        [TestMethod]
        public void PersonalPetsRepositoryTest()
        {
            // Arrange
            mockContextProvider.Setup(m => m.GetAll()).ReturnsAsync(testData);

            // Act
            PersonalPetsRepository personalPetsRepository = this.CreatePersonalPetsRepository();

            var task = personalPetsRepository.GetTGroupedSorted();
            var scopedResult = task.Result;
            Assert.IsNotNull(scopedResult);
            Assert.IsTrue(scopedResult.Keys.Count > 0, "GetTGroupedSorted has no groups (keys)");
            //Are the List<Pets> listed by gender? Gender will be key.
            Assert.IsTrue(scopedResult.Keys.Contains("Male"), "personalPetsRepository.GetTGroupedSorted isnt keyed by gender???");
            //Are all Pet "Cat" (and only Cat) in the scoped result as per spec? (6 cats testData)
            Assert.AreEqual(scopedResult.Values.Sum(x => x.Count), 6, "GetTGroupedSorted does not return correct number of 'Cat' results???");
            //Are the results ordered correctly as per spec
            foreach (var pets in scopedResult.Values)
            {
                var sorted = pets.OrderBy(x => x.Name).ToList();
                Assert.AreEqual(pets.First(), sorted.First(), "GetTGroupedSorted does not appear to provide correct sort order");
                Assert.AreEqual(pets.Last(), sorted.Last(), "GetTGroupedSorted does not appear to provide correct sort order");
            }
        }

        private PersonalPetsRepository CreatePersonalPetsRepository()
        {
            return new PersonalPetsRepository(
                this.mockContextProvider.Object);
        }
    }
}
