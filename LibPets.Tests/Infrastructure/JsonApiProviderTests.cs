using LibPets.Infrastructure;
using LibPets.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace LibPets.Tests.Infrastructure
{
    [TestClass]
    public class JsonApiProviderTests
    {
        [TestMethod]
        [TestCategory("SkipWhenLiveUnitTesting")]
        public void JsonApiProviderLiveAPITests()
        {
            // Arrange

            // Act
            JsonApiProvider<Person> provider = this.CreateProvider();
            var results = provider.GetAll().Result;
            // Assert
            Assert.IsNotNull(results, "Expected deserialized json from live source received null object (is source valid?)");

            Assert.IsInstanceOfType(results, typeof(IList<Person>), "GetAll did not return the correct model type <Person>");

        }

        private JsonApiProvider<Person> CreateProvider()
        {
            return new JsonApiProvider<Person>(@"http://agl-developer-test.azurewebsites.net/people.json");
        }
    }
}
