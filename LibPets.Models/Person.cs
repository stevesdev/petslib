﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibPets.Models
{
    public class Person
    {
        /// <summary>
        /// Person Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Person Gender
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Person Age
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// List of Pets <Pet> for this Person
        /// </summary>
        public ICollection<Pet> Pets { get; set; }
    }
}
