﻿namespace LibPets.Models
{
    public class Pet
    {
        /// <summary>
        /// Pet Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Pet Type ie Cat, Dog, Fish etc.
        /// </summary>
        public string Type { get; set; }
    }
}
